import React from 'react';
import PropTypes from 'prop-types';
import styles from './ModelNumber.css'; // eslint-disable-line no-unused-vars

const ModelNumber = ({helperClass=''}) => {
    return (
        <span className={`ModelNumber ${helperClass}`}>EI-108</span>
    ); 
};

ModelNumber.propTypes = {
    helperClass: PropTypes.string
};

export default ModelNumber;