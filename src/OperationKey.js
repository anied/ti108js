import React from 'react';
import PropTypes from 'prop-types';
import styles from './OperationKey.css'; // eslint-disable-line no-unused-vars
import Key from './Key';

const OperationKey = ({children, helperClass='', ...rest}) => {
    return (
        <Key helperClass={`OperationKey ${helperClass}`} {...rest}>
            <span className="OperationKey-Number">{children}</span>
        </Key>
    );
};

OperationKey.propTypes = {
    children: PropTypes.node,
    helperClass: PropTypes.string,
};

export default OperationKey;