import React from 'react';
import PropTypes from 'prop-types';

class ArrowKeyNavigationWrapper extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {children} = this.props;
        return (
            <React.Fragment>
                {children}
            </React.Fragment>
        );
    }
}

ArrowKeyNavigationWrapper.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};

export default ArrowKeyNavigationWrapper;