const keymap = {
    'insertToDisplay': ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.'],
    'clearDisplay': ['esc'],
    'evaluateExpression': ['=', 'enter'],
    'queueOperation': ['+', '-', '*', '/'],
    'memoryAdd': ['m p', 'm a'],
    'memoySubtract': ['m s'],
    'memoryRecall': ['m r'],
    'invertSign': ['i'],
    'squareRoot': ['s'],
    'percent': ['p', '%'],
    'navigateByArrowKeys': ['up', 'down', 'left', 'right'],
};

export default keymap;