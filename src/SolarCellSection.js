import React from 'react';
import styles from './SolarCellSection.css'; // eslint-disable-line no-unused-vars

import CompanyName from './CompanyName';
import ModelNumber from './ModelNumber';
import PhotovoltaicSolarCells from './PhotovoltaicSolarCells';

const PhotovoltaicSolarCell = () => {
    return (
        <div className="SolarCellSection">
            <CompanyName helperClass="Company" />
            <ModelNumber helperClass="Model"/>
            <PhotovoltaicSolarCells helperClass="SolarCells" />
        </div>
    );
};

export default PhotovoltaicSolarCell;