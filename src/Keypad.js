import React from 'react';
import PropTypes from 'prop-types';
import styles from './Keypad.css'; // eslint-disable-line no-unused-vars
import NumberPad from './NumberPad';
import AdvancedOperations from './AdvancedOperations';
import MemoryOperations from './MemoryOperations';
import BasicOperations from './BasicOperations';

class Keypad extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={`Keypad ${this.props.helperClass}`}>
                <AdvancedOperations
                    invertSign={this.props.invertSign}
                    squareRoot={this.props.squareRoot}
                    percent={this.props.percent}
                />
                <MemoryOperations
                    memoryAdd={this.props.memoryAdd}
                    memorySubtract={this.props.memorySubtract}
                    memoryRecall={this.props.memoryRecall}
                />
                <NumberPad
                    insertToDisplay={this.props.insertToDisplay}
                    clearDisplay={this.props.clearDisplay}
                />
                <BasicOperations
                    queueOperation={this.props.queueOperation}
                    evaluateExpression={this.props.evaluateExpression}
                />
            </div> 
        );
    }
}

Keypad.propTypes = {
    helperClass: PropTypes.string,
    insertToDisplay: PropTypes.func.isRequired,
    clearDisplay: PropTypes.func.isRequired,
    queueOperation: PropTypes.func.isRequired,
    evaluateExpression: PropTypes.func.isRequired,
    memoryAdd: PropTypes.func.isRequired,
    memorySubtract: PropTypes.func.isRequired,
    memoryRecall: PropTypes.func.isRequired,
    invertSign: PropTypes.func.isRequired,
    squareRoot: PropTypes.func.isRequired,
    percent: PropTypes.func.isRequired,
};

Keypad.defaultProps = {
    helperClass: '',
};

export default Keypad;