/* eslint-disable indent */
const xbounds = 3;
const ybounds = 5;
const map = [
    [{y: 0, x: 0}, {y: 0, x: 1}, {y: 0, x: 2}, {y: 0, x: 3}],
    [{y: 1, x: 0}, {y: 1, x: 1}, {y: 1, x: 2}, {y: 1, x: 3}],
    [{y: 2, x: 0}, {y: 2, x: 1}, {y: 2, x: 2}, {y: 2, x: 3}],
    [{y: 3, x: 0}, {y: 3, x: 1}, {y: 3, x: 2}, {y: 3, x: 3}],
    [{y: 4, x: 0}, {y: 4, x: 1}, {y: 4, x: 2}, {y: 4, x: 3}],
    [{y: 5, x: 0}, {y: 5, x: 1}, {y: 5, x: 2}, {y: 4, x: 3}],
];
export default function arrowKeyNavigationService(event) {
    const currentItem = document.activeElement;

    const spanY = currentItem.getAttribute('span-y');
    const spanX = currentItem.getAttribute('span-x');

    const currentPosition = {
        x: parseInt(currentItem.getAttribute('grid-x'), 10),
        y: parseInt(currentItem.getAttribute('grid-y'), 10),
    };
    let nextPosition = {...currentPosition};
    switch (event.key) {
        case 'ArrowUp':
            nextPosition.y -= 1;
            break;
        case 'ArrowDown':
            nextPosition.y += spanY || 1;
            break;
        case 'ArrowLeft':
            nextPosition.x -= 1;
            break;
        case 'ArrowRight':
            nextPosition.x += spanX || 1;
            break;
        default:
            break;
    }

    if (nextPosition.y > ybounds) {
        nextPosition.y = 0;
    }

    if (nextPosition.x > xbounds) {
        nextPosition.x = 0;
    }

    if (nextPosition.y < 0) {
        nextPosition.y = ybounds;
    }

    if (nextPosition.x < 0) {
        nextPosition.x = xbounds;
    }

    const position = map[nextPosition.y][nextPosition.x];
    const nextElement = document.querySelector(`[grid-x="${position.x}"][grid-y="${position.y}"`);

    if (nextElement) {
        nextElement.focus();
    }

}