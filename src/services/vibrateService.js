let vibrate = () => {/*noop*/};

if (window && window.navigator && window.navigator.vibrate) {
    vibrate = pattern => window.navigator.vibrate(pattern);
}

export default vibrate;