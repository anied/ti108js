/* This is somewhat of a superflous passthrough, but was retained for ease when refactoring. TODO remove */
import Big from 'big.js';
export const add = (a, b) => new Big(a).plus(new Big(b)).toString();
export const subtract = (a, b) => new Big(a).minus(new Big(b)).toString();
export const multiply = (a, b) => new Big(a).times(new Big(b)).toString();
export const divide = (a, b) => new Big(a).div(new Big(b)).toString();

const keystrokeRosettaStone = {
    '+': 'add',
    '-': 'subtract',
    '*': 'multiply',
    '/': 'divide',
};

export function mapKeystrokeToOperation(key) {
    return keystrokeRosettaStone[key];
}