import React, { Component } from 'react';
import {HotKeys} from 'react-hotkeys';
import keyMap from './keyboard/keyMap';

import Settings from './Settings';
import Calculator from './Calculator';
import './App.css';

class App extends Component {
    constructor() {
        super();
        this.state = {
            sizeModifier: 1,
            highContrastMode: false,
        };
        this.increaseSize = this.increaseSize.bind(this);
        this.decreaseSize = this.decreaseSize.bind(this);
        this.resetSize = this.resetSize.bind(this);
        this.toggleHighContrastMode = this.toggleHighContrastMode.bind(this);
    }
    increaseSize(e) {
        e.preventDefault();
        this.setState((prevState, props) => ({ // eslint-disable-line no-unused-vars
            sizeModifier: prevState.sizeModifier+0.1
        }));
    }
    decreaseSize(e) {
        e.preventDefault();
        this.setState((prevState, props) => ({ // eslint-disable-line no-unused-vars
            sizeModifier: prevState.sizeModifier-0.1
        }));
    }
    resetSize(e) {
        e.preventDefault();
        this.setState({
            sizeModifier: 1
        });
    }
    toggleHighContrastMode() {
        this.setState((prevState, props) => ({ // eslint-disable-line no-unused-vars
            highContrastMode: !prevState.highContrastMode
        }));
    }
    render() {
        const HighContrastMode = this.state.highContrastMode ? {highcontrastmode: 'true'} : {};
        return (
            <HotKeys keyMap={keyMap}>
                <div className="App" {...HighContrastMode}>
                    <Settings increaseSize={this.increaseSize} decreaseSize={this.decreaseSize} resetSize={this.resetSize} toggleHighContrastMode={this.toggleHighContrastMode} highContrastEnabled={this.state.highContrastMode}/>
                    <Calculator sizeModifier={this.state.sizeModifier}/>
                </div>
            </HotKeys>
        );
    }
}

export default App;
