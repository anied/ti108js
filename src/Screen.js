import React from 'react';
import PropTypes from 'prop-types';
import styles from './Screen.css'; // eslint-disable-line no-unused-vars

const Screen = ({display, itemInMemory, errorFlag}) => {
    return (
        <div className="DisplayScreen">
            <div className="DisplayLCD">
                {itemInMemory && <span className="MemoryIndicator">M</span>}
                {errorFlag && <span className="ErrorIndicator">E</span>}
                {display}
            </div>
        </div>
    );
};

Screen.propTypes = {
    display: PropTypes.string,
    itemInMemory: PropTypes.bool.isRequired,
    errorFlag: PropTypes.bool.isRequired,
};

export default Screen;