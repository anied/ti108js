import React from 'react';
import PropTypes from 'prop-types';
import styles from './AdvancedOperations.css'; // eslint-disable-line no-unused-vars
import OperationKey from './OperationKey';

const AdvancedOperations = ({invertSign, squareRoot, percent}) => {
    return (
        <div className="AdvancedOperations">
            <OperationKey onClick={invertSign} grid-x="0" grid-y="0">+/-</OperationKey>
            <OperationKey onClick={squareRoot} grid-x="1" grid-y="0">√x</OperationKey>
            <OperationKey onClick={percent} grid-x="2" grid-y="0">%</OperationKey>
        </div>
    );
};

AdvancedOperations.propTypes = {
    invertSign: PropTypes.func.isRequired,
    squareRoot: PropTypes.func.isRequired,
    percent: PropTypes.func.isRequired,
};

export default AdvancedOperations;