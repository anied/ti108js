import React from 'react';
import PropTypes from 'prop-types';
import styles from './BasicOperations.css'; // eslint-disable-line no-unused-vars
import OperationKey from './OperationKey';

const BasicOperations = ({queueOperation, evaluateExpression}) => {
    return (
        <div className="BasicOperations">
            <OperationKey onClick={() => queueOperation('divide')} grid-x="3" grid-y="0">&divide;</OperationKey>
            <OperationKey onClick={() => queueOperation('multiply')} grid-x="3" grid-y="1">&times;</OperationKey>
            <OperationKey onClick={() => queueOperation('subtract')} grid-x="3" grid-y="2">&minus;</OperationKey>
            <OperationKey onClick={() => queueOperation('add')} grid-x="3" grid-y="3">+</OperationKey>
            <OperationKey 
                helperClass="BasicOperations-EqualButton"
                onClick={() => evaluateExpression()}
                grid-x="3" grid-y="4"
                span-y="2"
            >
                =
            </OperationKey>
        </div>
    );
};

BasicOperations.propTypes = {
    queueOperation: PropTypes.func.isRequired,
    evaluateExpression: PropTypes.func.isRequired,
};

export default BasicOperations;