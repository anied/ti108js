import React from 'react';
import PropTypes from 'prop-types';
import styles from './Display.css'; // eslint-disable-line no-unused-vars
import Screen from './Screen';

class Display extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="Display">
                <div className="Display-Gutter" />
                <div className="Display-ScreenSection">
                    <Screen
                        display={this.props.display}
                        itemInMemory={this.props.itemInMemory}
                        errorFlag={this.props.errorFlag}
                    />
                </div>
                <div className="Display-Gutter" />
            </div>
        );
    }
}

Display.propTypes = {
    display: PropTypes.string,
    itemInMemory: PropTypes.bool.isRequired,
    errorFlag: PropTypes.bool.isRequired,
};

Display.defaultProps = {
    display: '0',
};

export default Display;