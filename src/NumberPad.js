import React from 'react';
import PropTypes from 'prop-types';
import styles from './NumberPad.css'; // eslint-disable-line no-unused-vars
import NumberKey from './NumberKey';
import OperationKey from './OperationKey';

const NumberPad = ({insertToDisplay, clearDisplay}) => {

    return (
        <div className="NumberPad">
            <NumberKey onClick={() => insertToDisplay('1')} grid-x="0" grid-y="2">1</NumberKey>
            <NumberKey onClick={() => insertToDisplay('2')} grid-x="1" grid-y="2">2</NumberKey>
            <NumberKey onClick={() => insertToDisplay('3')} grid-x="2" grid-y="2">3</NumberKey>
            <NumberKey onClick={() => insertToDisplay('4')} grid-x="0" grid-y="3">4</NumberKey>
            <NumberKey onClick={() => insertToDisplay('5')} grid-x="1" grid-y="3">5</NumberKey>
            <NumberKey onClick={() => insertToDisplay('6')} grid-x="2" grid-y="3">6</NumberKey>
            <NumberKey onClick={() => insertToDisplay('7')} grid-x="0" grid-y="4">7</NumberKey>
            <NumberKey onClick={() => insertToDisplay('8')} grid-x="1" grid-y="4">8</NumberKey>
            <NumberKey onClick={() => insertToDisplay('9')} grid-x="2" grid-y="4">9</NumberKey>
            <OperationKey helperClass="NumberPad-PowerButton" onClick={clearDisplay} grid-x="0" grid-y="5">ON/C</OperationKey>
            <NumberKey onClick={() => insertToDisplay('0')} grid-x="1" grid-y="5">0</NumberKey>
            <NumberKey onClick={() => insertToDisplay('.')} grid-x="2" grid-y="5">.</NumberKey>
        </div>
    );
};

NumberPad.propTypes = {
    insertToDisplay: PropTypes.func.isRequired,
    clearDisplay: PropTypes.func.isRequired,
};

export default NumberPad;