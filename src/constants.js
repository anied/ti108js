export const MAX_DISPLAY_LENGTH = 9;
export const QUEUEABLE_OPERATIONS = ['add', 'subtract', 'divide', 'multiply'];