import React from 'react';
import PropTypes from 'prop-types';
import './Settings.css';

export default function Settings({increaseSize, decreaseSize, resetSize, toggleHighContrastMode, highContrastEnabled}) {
    return (
        <div className="Settings">
            <div className="SizeSettings">
                <button onClick={decreaseSize}>-</button>
                <button onClick={resetSize}>RESET</button>
                <button onClick={increaseSize}>+</button>
            </div>
            <div className="ContrastSettings">
                <label htmlFor="highContrast">High Contrast<input type="checkbox" name="highContrast" onClick={toggleHighContrastMode} checked={highContrastEnabled}/></label>
            </div>
        </div>
    );
}

Settings.propTypes = {
    increaseSize: PropTypes.func.isRequired,
    decreaseSize: PropTypes.func.isRequired,
    resetSize: PropTypes.func.isRequired,
    toggleHighContrastMode: PropTypes.func.isRequired,
    highContrastEnabled: PropTypes.bool.isRequired
};