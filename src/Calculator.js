import React from 'react';
import PropTypes from 'prop-types';
import Big from 'big.js';
import {HotKeys} from 'react-hotkeys';
import styles from './Calculator.css'; // eslint-disable-line no-unused-vars
import { MAX_DISPLAY_LENGTH, QUEUEABLE_OPERATIONS } from './constants';
import * as basicOps from './services/basicOperations';
import arrowKeyNavigationService from './services/arrowKeyNavigationService';
import Display from './Display';
import SolarCellSection from './SolarCellSection';
import KeypadSection from './KeypadSection';

class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            display: '0',
            isNegative: false,
            operand: undefined,
            queuedOperation: undefined,
            clearNextInsert: false,
            memoryValue: 0,
            mrcFlag: false,
            errorFlag: false,
        };
        this.insertToDisplay = this.insertToDisplay.bind(this);
        this.clearDisplay = this.clearDisplay.bind(this);
        this.queueOperation = this.queueOperation.bind(this);
        this.evaluateExpression = this.evaluateExpression.bind(this);
        this.memoryAdd = this.memoryAdd.bind(this);
        this.memorySubtract = this.memorySubtract.bind(this);
        this.memoryRecall = this.memoryRecall.bind(this);
        this.invertSign = this.invertSign.bind(this);
        this.squareRoot = this.squareRoot.bind(this);
        this.percent = this.percent.bind(this);
        this.keyboardHandlers = {
            'insertToDisplay': event => this.insertToDisplay(event.key),
            'clearDisplay': this.clearDisplay,
            // 'queueOperation' // will require key to op translation
            'evaluateExpression': event => {
                event.preventDefault();
                this.evaluateExpression();
            },
            'queueOperation': event => {
                event.preventDefault();
                this.queueOperation(basicOps.mapKeystrokeToOperation(event.key));
            },
            'memoryAdd': this.memoryAdd,
            'memoySubtract': this.memoySubtract,
            'memoryRecall': this.memoryRecall,
            'invertSign': this.invertSign,
            'squareRoot': this.squareRoot,
            'percent': this.percent,
            'navigateByArrowKeys': event => arrowKeyNavigationService(event)
        };
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.errorFlag && nextState.errorFlag) {
            return false;
        }
        return true;
    }
    componentDidUpdate(prevProps, prevState, snapshot) { // eslint-disable-line no-unused-vars
        if (prevState.mrcFlag && this.state.mrcFlag) {
            this.setState((state, props) => ({mrcFlag: false})); // eslint-disable-line no-unused-vars
        }
    }
    insertToDisplay(character) {
        if (this.state.display.length < MAX_DISPLAY_LENGTH) {
            if (this.state.display === '0' || this.state.clearNextInsert) {
                this.setState((state, props) => ({ // eslint-disable-line no-unused-vars
                    display: character.toString(),
                    clearNextInsert: false,
                }));
            } else {
                this.setState((state, props) => ({display: state.display += character.toString()})); // eslint-disable-line no-unused-vars
            }
        }
    }
    clearDisplay() {
        this.setState((state, props) => ({ // eslint-disable-line no-unused-vars
            display: '0',
            errorFlag: false,
            cachedOperation: undefined,
        }));
    }
    queueOperation(operation) {
        const queueOp = () => this.setState((state, props) => ({ // eslint-disable-line no-unused-vars
            operand: state.display,
            queuedOperation: operation,
            clearNextInsert: true,
        }));

        if (QUEUEABLE_OPERATIONS.includes(operation)) {
            if (this.state.queuedOperation) {
                this.evaluateExpression(queueOp);
            } else {
                queueOp();
            }
        }
    }
    evaluateExpression(callback=()=>{/*noop*/}) {
        this.setState((state, props) => { // eslint-disable-line no-unused-vars
            let display;
            let cachedOperation = state.cachedOperation;
            let {queuedOperation: cachedOperator, display: cachedOperand} = state;
            if (state.queuedOperation) {
                display = basicOps[state.queuedOperation](state.operand, state.display).toString();
                cachedOperation = display => basicOps[cachedOperator](cachedOperand, display).toString();
            } else if (state.cachedOperation) {
                display = state.cachedOperation(state.display);
            } else {
                return {}; // TODO REVIEW
            }
            return {
                display,
                cachedOperation,
                operand: undefined,
                queuedOperation: undefined,
                clearNextInsert: true,
            };

        }, callback);
    }
    memoryAdd() {
        this.setState((state, props) => ({memoryValue: new Big(state.memoryValue).plus(new Big(state.display))})); // eslint-disable-line no-unused-vars
    }
    memorySubtract() {
        this.setState((state, props) => ({memoryValue: new Big(state.memoryValue).minus(new Big(state.display))})); // eslint-disable-line no-unused-vars
    }
    memoryRecall() {
        this.setState((state, props) => ({ // eslint-disable-line no-unused-vars
            display: state.memoryValue.toString(),
            mrcFlag: !state.mrcFlag,
            memoryValue: state.mrcFlag ? 0 : state.memoryValue,
            clearNextInsert: true
        })); 
    }
    invertSign() {
        this.setState((state, props) => ({display: new Big(state.display).times(new Big(-1)).toString()})); // eslint-disable-line no-unused-vars
    }
    squareRoot() {
        this.setState((state, props) => { // eslint-disable-line no-unused-vars
            const num = new Big(state.display);
            return {
                display: num.abs().sqrt().toString(),
                errorFlag: num.s === -1,
                clearNextInsert: true,
            };
        });
    }
    percent() {
        if (this.state.queuedOperation) {
            this.setState((state, props) => ({ // eslint-disable-line no-unused-vars
                display: new Big(state.operand).times(new Big(state.display).div(new Big(100))).toString()
            }));
        }
    }
    render() {
        const sizeModifier = this.props.sizeModifier;
        return (
            <HotKeys handlers={this.keyboardHandlers}>
                <div className="Calculator" style={{height: `calc(390px * ${sizeModifier})`, width: `calc(223px * ${sizeModifier})`, fontSize: `calc(1em * ${sizeModifier})`}}>
                    <Display 
                        display={this.state.display}
                        itemInMemory={this.state.memoryValue.toString() !== '0'}
                        errorFlag={this.state.errorFlag}
                    />
                    <SolarCellSection/>
                    <KeypadSection
                        helperClass="Calculator-Keypad"
                        insertToDisplay={this.insertToDisplay}
                        clearDisplay={this.clearDisplay}
                        queueOperation={this.queueOperation}
                        evaluateExpression={this.evaluateExpression}
                        memoryAdd={this.memoryAdd}
                        memorySubtract={this.memorySubtract}
                        memoryRecall={this.memoryRecall}
                        invertSign={this.invertSign}
                        squareRoot={this.squareRoot}
                        percent={this.percent}
                    />
                </div>
            </HotKeys>
        );
    }
}

Calculator.propTypes = {
    sizeModifier: PropTypes.number,
};

export default Calculator;