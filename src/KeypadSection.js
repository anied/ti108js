import React from 'react';
import PropTypes from 'prop-types';
import styles from './KeypadSection.css'; // eslint-disable-line no-unused-vars
import Keypad from './Keypad';

class KeypadSection extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={`KeypadSection ${this.props.helperClass}`}>
                <Keypad
                    insertToDisplay={this.props.insertToDisplay}
                    clearDisplay={this.props.clearDisplay}
                    queueOperation={this.props.queueOperation}
                    evaluateExpression={this.props.evaluateExpression}
                    memoryAdd={this.props.memoryAdd}
                    memorySubtract={this.props.memorySubtract}
                    memoryRecall={this.props.memoryRecall}
                    invertSign={this.props.invertSign}
                    squareRoot={this.props.squareRoot}
                    percent={this.props.percent}
                />
            </div>
        );
    }
}

KeypadSection.propTypes = {
    helperClass: PropTypes.string,
    insertToDisplay: PropTypes.func.isRequired,
    clearDisplay: PropTypes.func.isRequired,
    queueOperation: PropTypes.func.isRequired,
    evaluateExpression: PropTypes.func.isRequired,
    memoryAdd: PropTypes.func.isRequired,
    memorySubtract: PropTypes.func.isRequired,
    memoryRecall: PropTypes.func.isRequired,
    invertSign: PropTypes.func.isRequired,
    squareRoot: PropTypes.func.isRequired,
    percent: PropTypes.func.isRequired,
};

KeypadSection.defaultProps = {
    helperClass: '',
};

export default KeypadSection;