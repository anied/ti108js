import React from 'react';
import PropTypes from 'prop-types';
import vibrate from './services/vibrateService';
import style from './Key.css'; // eslint-disable-line no-unused-vars

const Key = ({children, helperClass='', onClick, ...rest}) => {
    return (
        <button 
            className={`KeyHOC ${helperClass}`}
            onClick={() => {
                vibrate(50);
                onClick();
            }}
            {...rest}
        >
            {children}
        </button>
    );
};

Key.propTypes = {
    children: PropTypes.node,
    helperClass: PropTypes.string,
    onClick: PropTypes.func,
};

export default Key;