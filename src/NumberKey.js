import React from 'react';
import PropTypes from 'prop-types';
import styles from './NumberKey.css'; // eslint-disable-line no-unused-vars
import Key from './Key';

const NumberKey = ({children, ...rest}) => {
    return (
        <Key helperClass="NumberKey" {...rest}>
            <span className="NumberKey-Number">{children}</span>
        </Key>
    );
};

NumberKey.propTypes = {
    children: PropTypes.node,
};

export default NumberKey;