import React from 'react';
import PropTypes from 'prop-types';
import styles from './CompanyName.css'; // eslint-disable-line no-unused-vars

const Title = ({helperClass=''}) => {
    return (
        <span className={`CompanyName ${helperClass}`}>ECMA Instruments</span>
    );
};

Title.propTypes = {
    helperClass: PropTypes.string
};

export default Title;