import React from 'react';
import PropTypes from 'prop-types';
import styles from './PhotovoltaicSolarCells.css'; // eslint-disable-line no-unused-vars

const PhotovoltaicSolarCells = ({helperClass=''}) => {
    return (
        <div className={`PhotovoltaicSolarCellsPanel ${helperClass}`}>
            <div className="Cell" />
            <div className="Cell" />
            <div className="Cell" />
            <div className="Cell" />
        </div>
    );
};

PhotovoltaicSolarCells.propTypes = {
    helperClass: PropTypes.string,
};

export default PhotovoltaicSolarCells;