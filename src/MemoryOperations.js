import React from 'react';
import PropTypes from 'prop-types';
import styles from './MemoryOperations.css'; // eslint-disable-line no-unused-vars
import OperationKey from './OperationKey';

const MemoryOperations = ({memoryAdd, memorySubtract, memoryRecall}) => {
    return (
        <div className="MemoryOperations">
            <OperationKey onClick={memoryRecall} grid-x="0" grid-y="1">MRC</OperationKey>
            <OperationKey onClick={memorySubtract} grid-x="1" grid-y="1">M-</OperationKey>
            <OperationKey onClick={memoryAdd} grid-x="2" grid-y="1">M+</OperationKey>
        </div>
    );
};

MemoryOperations.propTypes = {
    memoryAdd: PropTypes.func.isRequired,
    memorySubtract: PropTypes.func.isRequired,
    memoryRecall: PropTypes.func.isRequired,
};

export default MemoryOperations;