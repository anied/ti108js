# TODO

## Features

+ Sizable
+ Screen Reader with quick read
+ High Contrast
+ Mobile resizing
+ Error Range

## Bugs

+ Zero/Decimal
+ Limit screen width
+ Clear button mid-operation

## Tech Debt

+ Tests
+ Clean class components that should be functional components
